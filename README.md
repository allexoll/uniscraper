# UniScraper #

Project pour Labo_03 , S3 MD2

### objectifs: ###

La plateforme UniEmploi permet aux étudiants des UNIGE d’avoir accès à des offres d’emplois temporaires (pour les HES). Elle les classe par catégorie avec une mise en page HTML sans CSS. Il faut se connecter chaque jour pour voir si il y aurait une nouvelle offre qui nous intéresserait en allant regarder dans les catégories spécifiques. C’est un processus fastidieux, et les bonnes offres sont souvent prises rapidement. Le site n’a pas l’air d’utiliser de CSS :J’aimerai faire un script qui tournerai soit sur un raspberry pi, soit sur un beaglebone black, qui se connecte avec mes identifiants au site, et qui regarde s’il y a une nouvelle offre depuis la dernière fois, afin de m’envoyer une notification le cas échant. Ça me permettrait d’avoir un avantage sur les autres gens qui utilisent cette plateforme.J’imagine que le raspberry pi serait plus intéressant, vu le support important existant sur internet. Pareil pour le langage python, qui me permettrait de prototype sur mon mac avant de migrer le script sur le pi. Il faudrait faire de l’analyse HTML afin de trouver les liens des catégories dans le HTML, et un moyen d’envoyer des notifications sur mon téléphone. Après une courte recherche, je suis tombé sur https://instapush.im/. Ça a l’air de permettre de faire ça en python. Il faudrait aussi un moyen d’identifier les annonces de manière unique, pour faire la comparaison entre la précédente requête et l’actuelle. Il me semble que les bases de données ont un moyen de faire ça, si je me souviens vaguement de mes cours à l’hepia.Le service tournerait sur mon raspberry pi connecté à mon routeur, chez moi.
### so far: ###

* found package for form submission in HTML ([mechanize](https://pypi.python.org/pypi/mechanize/))
* only works with python2.7
* login to wadme working with my credentials (entered as argv as not to hardcode them in the python file


### TODO:###
* scrub categories list
* scrub offers and sort time, salary, content, and so on
* Unique ID for offers
* push to phone
* deploy on RPI


